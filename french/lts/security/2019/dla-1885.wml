#use wml::debian::translation-check translation="9623afbea568887444d6d1017f69074761395049" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18509">CVE-2017-18509</a>

<p>Denis Andzakovic a signalé une absence de vérification de type dans la mise
en œuvre du routage multicast IPv4. Un utilisateur doté de la capacité
CAP_NET_ADMIN (dans n'importe quel espace de noms) pourrait utiliser cela pour
un déni de service (corruption de mémoire ou plantage) ou éventuellement pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5995">CVE-2018-5995</a>

<p>ADLab de VenusTech a découvert que le noyau enregistrait les adresses
virtuelles assignées aux données par processeur, ce qui pourrait faciliter
l'exploitation d'autres vulnérabilités.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

<p>Chenxiang a signalé une situation de compétition dans libsas, le sous-système
du noyau prenant en charge les périphériques SAS (Serial Attached SCSI), qui
pourrait conduire à une utilisation de mémoire après libération. La manière dont
cela pourrait être exploité n'est pas évidente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20856">CVE-2018-20856</a>

<p>Xiao Jin a signalé une possible double libération de zone de mémoire dans le
sous-système de bloc au cas où une erreur survient lors de l'initialisation de
l'ordonnanceur d'entrée/sortie pour un périphérique bloc. La manière dont cela
pourrait être exploité n'est pas évidente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

<p>La plupart des processeurs x86 pourraient de façon spéculative ignorer une
instruction SWAPGS conditionnelle utilisée lorsqu'elle est entrée dans le noyau
à partir de l'espace utilisateur, et/ou pourraient l'exécuter de façon
spéculative alors qu'elle devrait être ignorée. Il s'agit d'un sous-type de
Spectre variante 1 qui pourrait permettre à des utilisateurs locaux d'obtenir
des informations sensibles du noyau ou d'autres processus. Le problème a été
pallié en utilisant des barrières de mémoire pour limiter l'exécution
spéculative. Les systèmes utilisant un noyau i386 ne sont pas affectés dans la
mesure où le noyau n'utilise pas d'instruction SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3882">CVE-2019-3882</a>

<p>Il a été découvert que l'implémentation de vfio ne limitait pas le nombre de
DMA mappant la mémoire du périphérique. Un utilisateur local à qui a été
octroyée la propriété d'un périphérique vfio pourrait utiliser cela pour
provoquer un déni de service (condition d'épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3900">CVE-2019-3900</a>

<p>Les pilotes vhost ne contrôlaient pas correctement la quantité de travail
effectué par les requêtes de service des VM client. Un client malveillant
pourrait utiliser cela pour provoquer un déni de service (usage illimité du
processeur) sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

<p>L'outil syzkaller a découvert un éventuel déréférencement de pointeur NULL
dans divers pilotes pour des adaptateurs Bluetooth avec une connexion UART. Un
utilisateur local doté d'un accès à un périphérique de pseudo-terminal ou un
autre périphérique tty adapté pourrait utiliser cela pour un déni de service
(bogue ou oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

<p>Amit Klein et Benny Pinkas ont découvert que la génération d'identifiants de
paquet IP utilisait une fonction de hachage faible, <q>jhash</q>. Cela
pourrait permettre de pister des ordinateurs particuliers alors qu'ils
communiquent avec divers serveurs distants et à partir de réseaux différents. La
fonction <q>siphash</q> est maintenant utilisée à la place.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

<p>Amit Klein et Benny Pinkas ont découvert que la génération d'identifiants de
paquet IP utilisait une fonction de hachage faible qui incorporait une adresse
virtuelle du noyau. Cette fonction de hachage n'est plus utilisée pour les
identifiants d'IP, même si elle est encore utilisée à d'autres fins dans la pile
de réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

<p>Le pilote gtco pour les tablettes graphiques USB pourrait provoquer un
débordement de tampon de pile avec des données constantes lors de l'analyse du
descripteur du périphérique. Un utilisateur physiquement présent avec un
périphérique USB spécialement construit pourrait utiliser cela pour provoquer un
déni de service (bogue ou oops), ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

<p>Praveen Pandey a signalé que sur les systèmes PowerPC (ppc64el) sans mémoire
transactionnelle (TM), le noyau pourrait néanmoins tenter de restaurer l'état de
TM passé à l'appel système sigreturn(). Un utilisateur local pourrait utiliser
cela pour un déni de service (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

<p>L'outil syzkaller a découvert une absence de vérification de limites dans le
pilote de disquette. Un utilisateur local doté d'un accès à un lecteur de
disquette, avec une disquette présente, pourrait utiliser cela pour lire la
mémoire du noyau au-delà du tampon d'entrée/sortie, obtenant éventuellement des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

<p>L'outil syzkaller a découvert une éventuelle division par zéro dans le pilote
de disquette. Un utilisateur local doté d'un accès à un lecteur de disquette
pourrait utiliser cela pour un déni de service (oops).</p></li>

<li><a href="https://pulsesecurity.co.nz/advisories/linux-kernel-4.9-tcpsocketsuaf">(identifiant CVE pas encore assigné)</a>

<p>Denis Andzakovic a signalé une utilisation possible de mémoire après
libération dans l'implémentation des sockets TCP. Un utilisateur local pourrait
utiliser cela pour un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation des privilèges.</p></li>

<li>(identifiant CVE pas encore assigné)

<p>Le sous-système conntrack de Netfilter utilisait des adresses du noyau comme
identifiants visibles par l'utilisateur, ce qui pourrait faciliter
l'exploitation d'autres vulnérabilités de sécurité.</p></li>

<li><a href="https://xenbits.xen.org/xsa/advisory-300.html">XSA-300</a>

<p>Julien Grall a signalé que Linux ne limite pas la quantité de mémoire qu'un
domaine tentera de faire gonfler, pas plus qu'il ne limite la quantité de
mémoire « foreign/grant map » qu'un client individuel peut consommer, menant à
des conditions de déni de service (pour l'hôte ou les clients).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.168-1+deb9u5~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1885.data"
# $Id: $
