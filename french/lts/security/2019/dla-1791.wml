#use wml::debian::translation-check translation="d13363ce93c5ea649711349c7bb4683e342ede10" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans faad2, le Freeware Advanced Audio
Coder.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20194">CVE-2018-20194</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-20197">CVE-2018-20197</a>

<p>Un traitement incorrect de reconfiguration implicite de mappage de canaux
aboutit à plusieurs problèmes de dépassement de tampon basé sur le tas. Ces
défauts peuvent être exploités par des attaquants distants pour provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20198">CVE-2018-20198</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-20362">CVE-2018-20362</a>

<p>Une validation d’entrée utilisateur insuffisante dans le module sbr_hfadj
aboutit à des problèmes de dépassements de tampon basé sur la pile. Ces défauts
peuvent être exploités par des attaquants distants pour provoquer un déni de
service ou un autre impact non précisé.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.7-8+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets faad2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1791.data"
# $Id: $
