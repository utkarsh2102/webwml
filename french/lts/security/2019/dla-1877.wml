#use wml::debian::translation-check translation="2fdf3298e069353bcb214ae1506a244d1cf89ab2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans otrs2, un système de
tickets à problèmes bien connu.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11563">CVE-2018-11563</a>

<p>Un attaquant connecté dans OTRS comme client peut utiliser l’écran
d’aperçu de tickets pour divulguer des informations internes d'article à leurs
tickets de clients.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12746">CVE-2019-12746</a>

<p>Un utilisateur connecté dans OTRS comme agent pouvait divulguer à son insu
son ID de session en partageant le lien d’un article de ticket embarqué avec
des parties tierces. Cet identifiant peut être alors éventuellement utilisé pour
se faire passer pour un utilisateur de l’agent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13458">CVE-2019-13458</a>

<p>Un attaquant connecté dans OTRS comme utilisateur de l’agent avec les
permissions adéquates peut exploiter des étiquettes OTRS dans des modèles pour
pour divulguer des mots de passe chiffrés d’utilisateurs.</p>


<p>À cause d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12248">CVE-2019-12248</a>,
la visualisation de pièces jointes de courriel n’était plus possible. Cette mise
à jour met en œuvre correctement la nouvelle option
Ticket::Fronted::BlockLoadingRemoteContent.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1+deb8u11.</p>
<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1877.data"
# $Id: $
