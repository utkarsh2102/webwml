#use wml::debian::translation-check translation="6263dc74f03e0f22c41da7846216e6fc69cd47ca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été trouvés dans QEMU, un émulateur rapide de
processeur (notamment utilisé dans la virtualisation KVM et HVM de Xen.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5126">CVE-2016-5126</a>

<p>Un dépassement de tampon basé sur le tas dans la fonction iscsi_aio_ioctl
dans block/iscsi.c dans QEMU permet aux utilisateurs de l’OS local invité de
provoquer un déni de service (plantage du processus QEMU) ou éventuellement
d’exécuter du code arbitraire à l'aide d'un appel asynchrone contrefait E/S
iSCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5403">CVE-2016-5403</a>

<p>La fonction virtqueue_pop dans hw/virtio/virtio.c dans QEMU permet aux
administrateurs de l’OS local invité de provoquer un déni de service
(consommation de mémoire et plantage du processus QEMU) en soumettant des
requêtes sans attendre leur achèvement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9375">CVE-2017-9375</a>

<p>QEMU, lorsque construit avec la prise en charge de l’émulation du contrôleur
USB xHCI, permet aux utilisateurs de l’OS local invité de provoquer un déni de
service (appel récursif infini) à l’aide de vecteurs impliquant le contrôle du
séquençage des descripteurs de transfert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12068">CVE-2019-12068</a>

<p>Dorsal de disque scsi QEMU : lsi : sortie de boucle infinie lors de
l’exécution d’un script.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12155">CVE-2019-12155</a>

<p>Interface_release_resource dans hw/display/qxl.c dans QEMU possède un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13164">CVE-2019-13164</a>

<p>Qemu-bridge-helper.c dans QEMU ne garantit pas que le nom de l’interface
réseau (obtenu à partir de bridge.conf ou d’une option --br=bridge) est limité
à la taille IFNAMSIZ, pouvant conduire à un contournement d’ACL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14378">CVE-2019-14378</a>

<p>Ip_reass dans ip_entrée.c dans libslirp 4.0.0 possède un dépassement de
tampon basé sur le tas à l'aide d'un grand paquet car il gère incorrectement un
cas impliquant le premier fragment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15890">CVE-2019-15890</a>

<p>Libslirp 4.0.0, comme utilisé dans QEMU, possède une utilisation de mémoire
après libération dans ip_reass dans ip_entrée.c.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1+dfsg-12+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1927.data"
# $Id: $
