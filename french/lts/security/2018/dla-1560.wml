#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un ensemble de vulnérabilités a été découvert dans GnuTLS qui permettait à
des attaquants des récupérations en texte simple lors de connexions TLS avec
certains types de chiffrement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10844">CVE-2018-10844</a>

<p>Il a été découvert que l’implémentation de GnuTLS de HMAC-SHA-256 était
vulnérable à une attaque de style «·Lucky thirteen·». Des attaquants distants
pourraient utiliser ce défaut pour conduire des attaques par caractéristiques
(distinguishing) et des attaques par récupération de texte simple
(plaintext-recovery) à l’aide d’analyses statistiques de données de minutage en
utilisant des paquets contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10845">CVE-2018-10845</a>

<p>Il a été découvert que l’implémentation de GnuTLS de HMAC-SHA-384 était
vulnérable à une attaque de style «·Lucky thirteen·». Des attaquants distants
pourraient utiliser ce défaut pour conduire des attaques par caractéristiques
(distinguishing) et des attaques par récupération de texte simple
(plaintext-recovery) à l’aide d’analyses statistiques de données de minutage en
utilisant des paquets contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10846">CVE-2018-10846</a>

<p>Un canal auxiliaire basé sur le cache dans l’implémentation de GnuTLS qui
conduisait à une récupération en texte simple dans la configuration d’attaque
«·cross-VM·» a été découvert. Un attaquant pourrait utiliser une
combinaison d’attaque <q>Just in Time</q> Prime+probe avec une combinaison
d’attaque Lucky-13 pour récupérer du texte simple en utilisant des paquets
contrefaits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
version 3.3.30-0+deb8u1. Il a été découvert qu’il était plus pratique de mettre
à jour vers la dernière version de l’amont de la branche 3.3.x puisque les
correctifs de l’amont étaient plutôt intrusifs et néanmoins demandaient des
modifications de listes de chiffrement. Cela facilitera aussi les mises à jour
futures pour LTS.</p>

<p>Cette modification par conséquent inclut aussi les modifications majeures de
politique suivantes, comme documentées dans le fichier NEWS :</p>

<ul>

<li>ARCFOUR (RC4) et SSL 3.0 ne sont plus inclus dans la liste des priorités
par défaut. Ceux-ci doivent être explicitement activés, par exemple, avec une
chaîne telle que «·NORMAL:+ARCFOUR-128·» ou «·NORMAL:+VERS-SSL3.0·»,
respectivement.</li>

<li>Les chiffrements utilisant HMAC-SHA384 et SHA256 doivent être retirés des
chaînes de priorités par défaut. Ils ne sont nécessaires pour la compatibilité
ou un autre but et n’offrent pas d’avantages par rapport à leurs contreparties
SHA1, car ils dépendent tous du mode ancien de bloc CBC TLS.</li>

<li>Suivi de près des recommandations de la RFC5280 et utilisation de UTCTime
pour les dates avant 2050.</li>

<li>Nécessité d’utiliser l’encodage DER strict pour les certificats, les
requêtes OCSP, les clefs privées, les requêtes de CRL et certificats, afin de
réduire les problèmes dûs à la complexité des règles BER.</li>

<li>Refus d’importation de certificats v1 ou v2 contenant des extensions.</li>

</ul>

<p>La compatibilité API et ABI est conservée, bien que de nouveaux symboles
aient été ajoutés. De nombreuses corrections sont aussi incluses dans la
publication. Consultez le journal des modifications fourni par l’amont pour
davantage de détails.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gnutls28 et n’espérons
pas de cassure importante.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1560.data"
# $Id: $
