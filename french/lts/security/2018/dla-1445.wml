#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Busybox, un ensemble d’utilitaires pour des systèmes petits et embarqués,
était affecté par plusieurs vulnérabilité de sécurité. Le projet « Common
vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2011-5325">CVE-2011-5325</a>

<p>Une vulnérabilité de traversée de répertoires a été découverte dans
l’implémentation par Busybox de tar. Tar extrait un lien symbolique pointant
en dehors du répertoire de travail en cours et puis suit ce lien symbolique lors
de l’extraction d’autres fichiers. Cela permet une attaque de traversée de
répertoires lors de l’extraction d’archives tar non fiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-1813">CVE-2013-1813</a>

<p>Lorsque un nœud de périphérique ou un lien symbolique dans /dev doit être
créé à l’intérieur d’un sous-répertoire niveau deux ou plus
(/dev/dir1/dir2.../nœud), les répertoires intermédiaires sont créés avec des
permissions incorrectes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4607">CVE-2014-4607</a>

<p>Une dépassement d'entier peut se produire lors du traitement de toute
variante de <q>literal run</q> dans la fonction lzo1x_decompress_safe. Chacun
de ces trois emplacements est susceptible d’un dépassement d'entier lors du
traitement de zéro octet. Cela expose le code copiant les <q>literal</q> à une
corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9645">CVE-2014-9645</a>

<p>La fonction add_probe dans modutils/modprobe.c dans BusyBox permet à des
utilisateurs locaux de contourner les restrictions voulues lors du chargement de
modules de noyau à l'aide d'un caractère / (slash) dans un nom de module, comme
démontré par une commande « ifconfig /usbserial up » ou « mount -t /snd_pcm none
/ ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2147">CVE-2016-2147</a>

<p>Un dépassement d’entier dans le client DHCP (udhcpc) dans BusyBox permet à
des attaquants distants de provoquer un déni de service (plantage) à l'aide d'un
nom de domaine malformé, encodé selon RFC-1035, qui déclenche une écriture de tas
hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2148">CVE-2016-2148</a>

<p>Un dépassement de tampon basé sur le tas dans le client DHCP (udhcpc) dans
BusyBox permet à des attaquants distants d’avoir un impact non précisé à l’aide
de vecteurs impliquant l’analyse de OPTION_6RD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15873">CVE-2017-15873</a>

<p>La fonction get_next_block dans archival/libarchive /decompress_bunzip2.c
dans BusyBox est sujette à un dépassement d’entier qui pourrait conduire à une
violation d’accès en l’écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16544">CVE-2017-16544</a>

<p>Dans la fonction add_match dans libbb/lineedit.c dans BusyBox, le fonction
d’autocomplètement tab de l’interpréteur, utilisé pour obtenir une liste de
noms de fichier d’un répertoire, ne vérifie pas les noms de fichier et aboutit
dans l’exécution de n’importe quelle séquence d’échappement dans le terminal.
Cela pourrait éventuellement aboutir à l’exécution de code, l’écriture de
fichiers arbitraires ou d’autres attaques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000517">CVE-2018-1000517</a>

<p>BusyBox contient une vulnérabilité de dépassement de tampon dans sa version de wget
pouvant aboutir dans un dépassement de tampon basé sur le tas. Cette attaque
semble être exploitable à l’aide de la connectivité réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9621">CVE-2015-9621</a>

<p>La décompression d’un fichier zip spécialement contrefait aboutit dans le
calcul d’un pointeur non valable et un plantage en lisant une adresse non
valable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:1.22.0-9+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets busybox.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1445.data"
# $Id: $
