msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:56+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "arapski"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "armenski"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "finski"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "hrvatski"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "danski"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "nizozemski"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "engleski"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "francuski"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "galicijski"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "njemački"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "talijanski"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "japanski"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "korejski"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "španjolski"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "portugalski"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "portugalski (brazilski)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "kineski"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "kineski (Kina)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "kineski (Hong Kong)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "kineski (Tajvan)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "kineski (tradicionalni)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "kineski (pojednostavljen)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "švedski"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "poljski"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "norveški"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "turski"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "ruski"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "češki"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "esperanto"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "mađarski"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "rumunjski"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "slovački"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "grčki"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "katalonski"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "indonezijski"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "litavski"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "slovenski"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "bugarski"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "tamilski"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "afrikaans"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "albanski"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr ""

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "amharski"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "azerbejdžanski"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "baskijski"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "bjeloruski"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "bengalski"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "bosanski"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "bretonski"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "cornski (keltski)"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "estonski"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "farski"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "galski (Škoti)"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "gruzijski"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "hebrejski"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "hindi"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "islandski"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "irski"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "grenlandski"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "kannada"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "kurdski"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "latvijski"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "makedonski"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "malajski"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "malayalam"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "malteški"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "manx"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "maorski"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "mongolski"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "norveški bokm&aring;l"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "norveški nynorsk"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "Occitan (nakon 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "perzijski"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "srpski"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "slovenski"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "tadžikski"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "tajlandski"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "tongaški"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "ukrajinski"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "vijetnamski"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "velški"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "Xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "jidiš"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "zulu"

#~ msgid "Gallegan"
#~ msgstr "galicijski"
