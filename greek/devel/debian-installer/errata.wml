#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5863207a091bcfec9124de413b37739d67e43557" maintainer="galaxico"

<h1>Errata for <humanversion /></h1>

<p>
This is a list of known problems in the <humanversion /> release of the Debian
Installer. If you do not see your problem listed here, please send us an
<a href="$(HOME)/releases/stable/amd64/ch05s04.html#submit-bug">installation
report</a> describing the problem.
</p>

<dl class="gloss">
     <dt>GNOME may fail to start with some virtual machine setups</dt>
     <dd>It was noticed during Stretch Alpha 4 image testing that
     GNOME might fail to start depending on the setup used for virtual
     machines. It seems using cirrus as an emulated video chip is fine.
     <br />
     <b>Status:</b> Being investigated.</dd>

     <dt>Desktop installations may not work using CD#1 alone</dt>
     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead. <br /> <b>Status:</b> It is
     unlikely more efforts can be made to fit more packages on
     CD#1. </dd>

     <dt>Theme used in the installer</dt>
     <dd>There's no Buster artwork yet, and the installer is still
       using the Stretch theme.
     <br />
     <b>Status:</b>
       Fixed in Buster RC 1: <a href="https://bits.debian.org/2019/01/futurePrototype-will-be-the-default-theme-for-debian-10.html">futurePrototype</a> has been integrated.</dd>

     <dt>Buggy shortcuts in the nano editor</dt>
     <dd>The nano version used inside Debian Installer doesn't
       advertise the same shortcuts as the regular nano package (on
       installed systems). In particular the suggestion to
       use <tt>C-s</tt> to <q>Save</q> seems to be a problem for
       people using the installer on a serial console
       (<a href="https://bugs.debian.org/915017">#915017</a>).
     <br />
     <b>Status:</b> Fixed in Buster RC 1.</dd>

     <dt>TLS support in wget is broken</dt>
     <dd>A change in the openssl library leads to some issues for its
       users, since a configuration file that was initially optional
       suddenly became mandatory. As a consequence, at
       least <tt>wget</tt> doesn't support HTTPS within the installer
       (<a href="https://bugs.debian.org/926315">#926315</a>).
     <br />
     <b>Status:</b> Fixed in Buster RC 2.</dd>

     <dt>Entropy-related issues</dt>
     <dd>Even with functional HTTPS support in wget, one might run
       into entropy-related issues, depending on the availability of a
       hardware random number generator (RNG) and/or on a sufficient
       number of events to feed the entropy pool
       (<a href="https://bugs.debian.org/923675">#923675</a>). Usual
       symptoms can be a long delay when the first HTTPS connection
       happens, or during SSH keypair generation.
     <br />
     <b>Status:</b> Fixed in Buster RC 2.</dd>

     <dt>LUKS2 is incompatible with GRUB's cryptodisk support</dt>
     <dd>It was only lately discovered that GRUB has no support for
       LUKS2. This means that users who want to use
       <tt>GRUB_ENABLE_CRYPTODISK</tt> and avoid a separate,
       unencrypted <tt>/boot</tt>, won't be able to do so
       (<a href="https://bugs.debian.org/927165">#927165</a>).  This
       setup isn't supported in the installer anyway, but it would
       make sense to at least document this limitation more
       prominently, and to have at least the possibility to opt for
       LUKS1 during the installation process.
     <br />
     <b>Status:</b> Some ideas have been expressed on the bug; cryptsetup maintainers have written <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">some specific documentation</a>.</dd>

     <dt>Failure to preseed custom APT repositories</dt>
     <dd>It's customary to also specify extra keys when configuring
       custom APT repositories, using parameters
       like <tt>apt-setup/local0/key</tt>. Due to changes in the
       way <tt>apt-key</tt> works, trying to specify such a setting
       would result in failures with the Buster installer
       (<a href="https://bugs.debian.org/851774">851774</a>).
     <br />
     <b>Status:</b>
       The <a href="https://bugs.debian.org/851774#68">proposed
       patch</a> wasn't reviewed in time for the initial Buster release, but is expected
       to be tested during the Bullseye release cycle and to be
       backported in a Buster point release eventually.</dd>

<!-- things should be better starting with Jessie Beta 2...
	<dt>GNU/kFreeBSD support</dt>

	<dd>GNU/kFreeBSD installation images suffer from various
	important bugs
	(<a href="https://bugs.debian.org/757985"><a href="https://bugs.debian.org/757985">#757985</a></a>,
	<a href="https://bugs.debian.org/757986"><a href="https://bugs.debian.org/757986">#757986</a></a>,
	<a href="https://bugs.debian.org/757987"><a href="https://bugs.debian.org/757987">#757987</a></a>,
	<a href="https://bugs.debian.org/757988"><a href="https://bugs.debian.org/757988">#757988</a></a>). Porters
	could surely use some helping hands to bring the installer back
	into shape!</dd>
-->

<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce...
	<dt>Accessibility of the installed system</dt>
	<dd>Even if accessibility technologies are used during the
	installation process, they might not be automatically enabled
	within the installed system.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>Desktop installations on i386 do not work using CD#1 alone</dt>
	<dd>Due to space constraints on the first CD, not all of the expected GNOME desktop
	packages fit on CD#1. For a successful installation, use extra package sources (e.g.
	a second CD or a network mirror) or use a DVD instead.
	<br />
	<b>Status:</b> It is unlikely more efforts can be made to fit more packages on CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Potential issues with UEFI booting on amd64</dt>
	<dd>There have been some reports of issues booting the Debian Installer in UEFI mode
	on amd64 systems. Some systems apparently do not boot reliably using <code>grub-efi</code>, and some
	others show graphics corruption problems when displaying the initial installation splash
	screen.
	<br />
	If you encounter either of these issues, please file a bug report and give as much detail
	as possible, both about the symptoms and your hardware - this should assist the team to fix
	these bugs. As a workaround for now, try switching off UEFI and installing using <q>Legacy
	BIOS</q> or <q>Fallback mode</q> instead.
	<br />
	<b>Status:</b> More bug fixes might appear in the various Wheezy point releases.
	</dd>
-->

<!-- ditto...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
