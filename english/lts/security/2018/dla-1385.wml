<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Man Yue Mo discovered a security vulnerability in Apache Batik, an SVG
image library. A missing check for the class type before calling
newInstance when deserializing a subclass of AbstractDocument could
lead to information disclosure.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.7+dfsg-3+deb7u3.</p>

<p>We recommend that you upgrade your batik packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1385.data"
# $Id: $
