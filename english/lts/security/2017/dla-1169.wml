<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability has been found in postgresql-common, Debian's
PostgreSQL database cluster management tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8806">CVE-2017-8806</a>

    <p>It was discovered that the pg_ctlcluster, pg_createcluster and
    pg_upgradecluster commands handled symbolic links insecurely which
    could result in local denial of service by overwriting arbitrary
    files.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
134wheezy6.</p>

<p>We recommend that you upgrade your postgresql-common packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1169.data"
# $Id: $
