<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>On a server redirect from HTTP to a FTP resource, wget would trust
the HTTP server and uses the name in the redirected URL as the
destination filename.
This behaviour was changed and now it works similarly as a redirect
from HTTP to another HTTP resource so the original name is used as
the destination file.  To keep the previous behaviour the user must
provide --trust-server-names.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.13.4-3+deb7u3.</p>

<p>We recommend that you upgrade your wget packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-536.data"
# $Id: $
