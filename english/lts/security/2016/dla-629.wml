<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Lukas Reschke discovered that Apache Jackrabbit, a content repository
implementation for Java, was vulnerable to Cross-Site-Request-Forgery
in Jackrabbit's webdav module.</p>

<p>The CSRF content-type check for POST requests did not handle missing
Content-Type header fields, nor variations in field values with
respect to upper/lower case or optional parameters. This could be
exploited to create a resource via CSRF.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.3.6-1+deb7u2.</p>

<p>We recommend that you upgrade your jackrabbit packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-629.data"
# $Id: $
