msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr ""

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr ""

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr ""

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr ""

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr ""

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr ""

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:55
msgid "Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal A Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:61
msgid "Proposal B Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal C Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal D Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal E Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal F Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:76
msgid "Opposition"
msgstr ""

#: ../../english/template/debian/votebar.wml:79
msgid "Text"
msgstr ""

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal A"
msgstr ""

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal B"
msgstr ""

#: ../../english/template/debian/votebar.wml:88
msgid "Proposal C"
msgstr ""

#: ../../english/template/debian/votebar.wml:91
msgid "Proposal D"
msgstr ""

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal E"
msgstr ""

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal F"
msgstr ""

#: ../../english/template/debian/votebar.wml:100
msgid "Choices"
msgstr ""

#: ../../english/template/debian/votebar.wml:103
msgid "Amendment Proposer"
msgstr ""

#: ../../english/template/debian/votebar.wml:106
msgid "Amendment Seconds"
msgstr ""

#: ../../english/template/debian/votebar.wml:109
msgid "Amendment Text"
msgstr ""

#: ../../english/template/debian/votebar.wml:112
msgid "Amendment Proposer A"
msgstr ""

#: ../../english/template/debian/votebar.wml:115
msgid "Amendment Seconds A"
msgstr ""

#: ../../english/template/debian/votebar.wml:118
msgid "Amendment Text A"
msgstr ""

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer B"
msgstr ""

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds B"
msgstr ""

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text B"
msgstr ""

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer C"
msgstr ""

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds C"
msgstr ""

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text C"
msgstr ""

#: ../../english/template/debian/votebar.wml:139
msgid "Amendments"
msgstr ""

#: ../../english/template/debian/votebar.wml:142
msgid "Proceedings"
msgstr ""

#: ../../english/template/debian/votebar.wml:145
msgid "Majority Requirement"
msgstr ""

#: ../../english/template/debian/votebar.wml:148
msgid "Data and Statistics"
msgstr ""

#: ../../english/template/debian/votebar.wml:151
msgid "Quorum"
msgstr ""

#: ../../english/template/debian/votebar.wml:154
msgid "Minimum Discussion"
msgstr ""

#: ../../english/template/debian/votebar.wml:157
msgid "Ballot"
msgstr ""

#: ../../english/template/debian/votebar.wml:160
msgid "Forum"
msgstr ""

#: ../../english/template/debian/votebar.wml:163
msgid "Outcome"
msgstr ""

#: ../../english/template/debian/votebar.wml:167
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr ""

#: ../../english/template/debian/votebar.wml:170
msgid "In&nbsp;Discussion"
msgstr ""

#: ../../english/template/debian/votebar.wml:173
msgid "Voting&nbsp;Open"
msgstr ""

#: ../../english/template/debian/votebar.wml:176
msgid "Decided"
msgstr ""

#: ../../english/template/debian/votebar.wml:179
msgid "Withdrawn"
msgstr ""

#: ../../english/template/debian/votebar.wml:182
msgid "Other"
msgstr ""

#: ../../english/template/debian/votebar.wml:186
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr ""

#: ../../english/template/debian/votebar.wml:189
msgid "How&nbsp;To"
msgstr ""

#: ../../english/template/debian/votebar.wml:192
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr ""

#: ../../english/template/debian/votebar.wml:195
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr ""

#: ../../english/template/debian/votebar.wml:198
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr ""

#: ../../english/template/debian/votebar.wml:201
msgid "Read&nbsp;a&nbsp;Result"
msgstr ""

#: ../../english/template/debian/votebar.wml:204
msgid "Vote"
msgstr ""

