<define-tag pagetitle>The Debian Project mourns the loss of Lucy Wayland</define-tag>
<define-tag release_date>2019-03-08</define-tag>
#use wml::debian::news
# $Id$

<p>The Debian Project sadly announces that it has lost a member of its
community. Lucy Wayland passed recently.</p>

<p>Lucy was a contributor within the Cambridge (UK) Debian community, helping
to organise the Cambridge Mini-DebConf since several years. 
</p>

<p>
She was a strong fighter for diversity and inclusion, and participated
in the creation of the Debian Diversity Team, working on increasing the
visibility of under-represented groups and providing support with respect to
diversity issues within the community. 
</p>

<p>The Debian Project honours her good work and strong dedication to Debian
and Free Software. Lucy's contributions will not be forgotten, and the
high standards of her work will continue to serve as an inspiration to
others.</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
