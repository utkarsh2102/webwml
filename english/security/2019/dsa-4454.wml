<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in QEMU, a fast processor
emulator, which could result in denial of service, the execution of
arbitrary code or information disclosure.</p>

<p>In addition this update backports support to passthrough the new
md-clear CPU flag added in the intel-microcode update shipped in DSA 4447
to x86-based guests.</p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 1:2.8+dfsg-6+deb9u6.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4454.data"
# $Id: $
